
build: node_modules
	node_modules/.bin/metalsmith && cp src/error-pages/* public/

node_modules: package.json
	npm install

.PHONY: build