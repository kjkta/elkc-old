---
layout: base.html
---

# Eastern Lions Kart Club

## Postal Address
PO Box 11  
Montrose, 3765  
A.B.N. 31 960 235 541  

**Contact Details**  
Mobile - 0411 218 208  
Other Mobile - 0438 570 755  
Email - general@elkc.com.au  

<div class="flex">

<div>
**President**  
Steve Pegg  
president@elkc.com.au  
0438 570 755  
</div>

<div>
**Vice President**  
Ross Harrod  
vp@elkc.com.au  
0418 511 140  
</div>

<div>
**Secretary**  
Gail Cherry  
secretary@elkc.com.au  
0411 218 208  
</div>

<div>
**Treasurer**  
David Thomson  
treasurer@elkc.com.au  
0419 208771  
</div>

<div>
**Membership Secretary**  
Ian Branson  
membsec@elkc.com.au   
0419 882 132  
</div>

<div>
**Race Entry**  
Lorraine Cubbin  
raceentry@elkc.com.au  
0408 576 193  
</div>

<div>
**VKA Delegates**  
Brett Hughes  
Justin Paragreen  
vkadelegates@elkc.com.au  
0414 912 988    
0434 312 777
</div>

<div>
**Website**  
Tom Mills  
web@elkc.com.au  
0467346532  
</div>

<div>
**Trophies**  
Glen Chadwick  
trophies@elkc.com.au  
0418 347 512  
</div>

<div>
**Race Committee**  
Ross Harrod  
Sarge Wilson  
Brendon Myors  
Ian Branson  
Daniel Pegg  
Gary Heywood  
Justin Paragreen  
racecommittee@elkc.com.au  
0418 511 140  
</div>

<div>
**Track & Grounds**  
Michael O'Brien  
track@elkc.com.au  
0410 527 076  
</div>

<div>
**Canteen**.  
Michael and Julie O'Brien   
canteen@elkc.com.au   
0410 527 076   
</div>

</div>
![ELKC Map](images/elkc_map.gif)