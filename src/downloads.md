---
layout: base.html
---

# Downloads

[2016 Karting Australia Rule Book](docs/downloads/2016 KA Manual.pdf)

[2017 ELKC MEMBERSHIP APPLICATION](docs/downloads/2017_Membership_Application_Form.pdf)

[ELKC 2014 Constitution](docs/downloads/Constitution_2014 final.pdf)

[ELKC 2014 Rules & Regulations](docs/downloads/ELKC Rules and Regulations_2014_V1.pdf)

[NEW DEALER TRADE LICENCE APPLICATION](docs/downloads/dealertradeapplication.pdf)

[WANT TO BE A KARTER?](docs/downloads/A testament to Karting.pdf)

[ACCOMMODATION](docs/downloads/ACCOMMODATION.pdf)

[CHASSIS TUNING](docs/downloads/Chassis tuning for good turn in.pdf)

[ELKC TRACK DETAILS](docs/downloads/ELKC Track Details.pdf)

[TRACK MAP](docs/downloads/arial map.JPG)

[Oil Ratio Calculations](docs/downloads/OIL RATIO CHART.pdf)

[Gearing Tables](docs/downloads/Gearing Ratio Table.pdf)

[Pit Crew Waiver Form](docs/downloads/Pit Crew Waiver Form.pdf)

[Official Kart Scrutineering Checklist](docs/downloads/OFFICIAL SCRUTINEERING SHEET.pdf)

[Official AKA race meeting Entry Form](docs/downloads/AKA Entry_Form.pdf)
