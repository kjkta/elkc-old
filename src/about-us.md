---
layout: base.html
---

# Who are the Eastern Lions Kart Club?

The Eastern Lions Kart Club was established in 1961, located as a track at Humevale near Whittlesea. The memberhip consists of approximately 160 karters and their families and is drawn from predominantly the Northern and Northern Eastern Suburbs of Melbourne and a growing regional membership. It is an incorporated club with a committee drawn from volunteer members within the club.

ELKC Aerial viewThe previous site at Whittlesea had been outgrown in terms of both track and surroundings. The need to move to a bigger location and longer track became apparent as long ago as 1989, and since then a number of sites had been looked at and evaluated. The opportunity to purchase 72 acres of land at Puckapunyal was ideal. The size of the land, zoning and proximity to facilities in Seymour and it's geographical location in Victoria all combined to make the location an excellent venue for a kart track.

![ELKC Track](images/track.jpg)

Being located only eight kilometres from Seymour, accommodation will not be a problem. Karters can travel to Kilmore, Nagambie or Broadford as alternatives. These local communities will benefit from the facility. Fuel supplies are also available close by.