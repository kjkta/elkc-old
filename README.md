[![build status](https://gitlab.com/kjkta/elkc/badges/master/build.svg)](https://gitlab.com/kjkta/elkc/commits/master)

# ELKC

## Edit a page

- Go to `src` folder --> Select page to edit with the file extention `.md`.
- Click `Edit` button in the top right corner.
- Here you can edit the contents of the page. 
- You can add text by just typing it in, and optionally style text, add images, links, lists and lots more with Markdown (see below).
- To save, click "Commit Changes" button at the bottom.  


### What's Markdown?

Here is a reference of all the things you can do: https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet

In short, you can add Headings like this;
```
# Heading Text
## Heading Text
### Heading Text
```

Links like this;
`[This is the link text](https://www.this-is-the-url.com)`

Images like this;
`![the name of the image](images/name_of_image_file.jpg)`  


## Upload a file or image (ie. suppregs, downloads)

- Go to `src` --> `suppregs` or `downloads` / other
- Click the `+` next to the directory path
- Select `Upload a file`
- Link the file into one of the website pages (eg. `[2016 Karting Australia Rule Book](docs/downloads/2016 KA Manual.pdf)`)  
  
  
  



---

# Metalsmith Docs

---

![Build Status](https://gitlab.com/pages/metalsmith/badges/master/build.svg)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install metalsmith
  - make build
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Metalsmith
1. Generate the website: `make build`
1. Add content

Read more at Metalsmith's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @VeraKolotyuk


[ci]: https://about.gitlab.com/gitlab-ci/
[metalsmith]: http://www.metalsmith.io/
[install]: http://www.metalsmith.io/#install-it
[documentation]: http://www.metalsmith.io
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
